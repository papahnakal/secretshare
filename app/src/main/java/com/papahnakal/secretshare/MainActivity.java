package com.papahnakal.secretshare;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView txtLogReg;
    private Button btn_login,btn_register;
    private EditText edit_id,edit_pass;
    private SharedPreferences sp,spLogin;
    private LinearLayout lyt_login;
    private static final String PREFERENCE = "PREFERENCE";
    private static final String LOGIN_PREFERENCE = "LOG_PREFERENCE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.activity_main);
        castingObject();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        sp = getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        spLogin = getSharedPreferences(LOGIN_PREFERENCE,Context.MODE_PRIVATE);
        checkID();
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogin();
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doRegister();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void checkID(){
        if(!sp.getString("idSaved","").equalsIgnoreCase("")){
            if(!spLogin.getString("id","").equalsIgnoreCase("")){
                Intent toContent = new Intent(MainActivity.this,MainSecretShare.class);
                startActivity(toContent);
                finish();
            }else{
                enableLogin();
            }
        }else{
            enableRegister();
        }
    }
    public void enableRegister(){
        txtLogReg.setText("REGISTER");
        if(btn_register.getVisibility()== View.GONE){
            btn_register.setVisibility(View.VISIBLE);
            btn_login.setVisibility(View.GONE);
        }
    }
    public void enableLogin(){
        txtLogReg.setText("LOGIN");
        if(btn_login.getVisibility()== View.GONE){
            btn_login.setVisibility(View.VISIBLE);
            btn_register.setVisibility(View.GONE);
        }
    }
    public void doLogin(){
        String idSaved = sp.getString("idSaved","");
        String passSaved = sp.getString("passSaved","");
        if(idSaved.equalsIgnoreCase(edit_id.getText().toString())
                &&passSaved.equalsIgnoreCase(edit_pass.getText().toString())){
            SharedPreferences.Editor editor = spLogin.edit();
            editor.putString("id",edit_id.getText().toString());
            editor.putString("password",edit_pass.getText().toString());
            editor.commit();
            Intent toContent = new Intent(MainActivity.this,MainSecretShare.class);
            startActivity(toContent);
            finish();
        }else{
            Toast.makeText(MainActivity.this,"Maaf id password salah!",Toast.LENGTH_SHORT).show();
            edit_id.setText("");
            edit_pass.setText("");
        }
    }
    public void doRegister(){
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("idSaved",edit_id.getText().toString());
        editor.putString("passSaved",edit_pass.getText().toString());
        editor.commit();
        edit_id.setText("");
        edit_pass.setText("");
        enableLogin();
    }
    public void castingObject(){
        lyt_login = (LinearLayout)findViewById(R.id.lyt_login);
        lyt_login.setVisibility(View.VISIBLE);
        txtLogReg = (TextView)findViewById(R.id.txt_title_reglog);
        btn_login = (Button)findViewById(R.id.btn_login);
        btn_register = (Button)findViewById(R.id.btn_register);
        edit_id = (EditText)findViewById(R.id.edt_id);
        edit_pass = (EditText)findViewById(R.id.edt_pass);
    }
}
