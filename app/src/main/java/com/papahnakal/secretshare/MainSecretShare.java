package com.papahnakal.secretshare;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

/**
 * Created by skyshi on 22/01/16.
 */
public class MainSecretShare extends AppCompatActivity {
    private RelativeLayout lyt_content;
    private SharedPreferences sp,spLogin;
    private Button btn_logout,btn_encrypt,btn_decrypt;
    private EditText input,output,key;
    private ImageView img_share,img_copy,img_refresh;
    private static final String PREFERENCE = "PREFERENCE";
    private static final String LOGIN_PREFERENCE = "LOG_PREFERENCE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setContentView(R.layout.activity_main);
        castingObject();
        sp = getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        spLogin = getSharedPreferences(LOGIN_PREFERENCE,Context.MODE_PRIVATE);
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor1 = spLogin.edit();
                editor1 = spLogin.edit();
                editor1.clear();
                editor1.commit();
                Intent logout = new Intent(MainSecretShare.this,MainActivity.class);
                startActivity(logout);
                finish();
            }
        });
        btn_encrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!key.getText().toString().equalsIgnoreCase("") &&
                        !input.getText().toString().equalsIgnoreCase("")) {
                    String al = toAll4y(input.getText().toString());
                    output.setText(enkrip(al, Integer.parseInt(key.getText().toString())));
                }else{
                    if(key.getText().toString().equalsIgnoreCase("")){
                        Toast.makeText(MainSecretShare.this, "your key '" + key.getText().toString() + "' is not valid!", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(MainSecretShare.this, "please, input text.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        btn_decrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String keys ="";
                String text = "";
                if(!input.getText().toString().equalsIgnoreCase("")) {
                    if (input.getText().toString().substring(0, 1).equalsIgnoreCase("0")) {
                        keys = input.getText().toString().substring(input.getText().toString().length() - 1);
                        text = input.getText().toString().substring(0, input.getText().toString().length() - 1);
                        Log.d("key", "one" + keys);
                    } else if (!input.getText().toString().substring(input.getText().toString().length() - 1).equalsIgnoreCase(".")) {
                        keys = input.getText().toString().substring(0, 1) +
                                input.getText().toString().substring(input.getText().toString().length() - 1);
                        text = input.getText().toString().substring(1, input.getText().toString().length() - 1);
                        Log.d("key", "two" + keys);
                    } else if (input.getText().toString().substring(input.getText().toString().length() - 1).equalsIgnoreCase(".")) {
                        if (input.getText().toString().substring(input.getText().toString().length() - 2).equalsIgnoreCase("..")) {
                            keys = input.getText().toString().substring(0, 2) +
                                    input.getText().toString().substring(input.getText().toString().length() - 4,
                                            input.getText().toString().length() - 2);
                            text = input.getText().toString().substring(2, input.getText().toString().length() - 4);
                            Log.d("key", "three" + keys);
                        } else {
                            keys = input.getText().toString().substring(0, 2) +
                                    input.getText().toString().substring(input.getText().toString().length() - 2,
                                            input.getText().toString().length() - 1);
                            text = input.getText().toString().substring(2, input.getText().toString().length() - 2);
                            Log.d("key", "four" + keys);
                        }
                    }
                }else{
                    if(input.getText().toString().equalsIgnoreCase("")) {
                        Toast.makeText(MainSecretShare.this, "please, input encrypted text.", Toast.LENGTH_SHORT).show();
                    }/*else{
                        Toast.makeText(MainSecretShare.this, "please, input key ecrypt.", Toast.LENGTH_SHORT).show();
                    }*/
                }
                // output.getText().toString().substring(1,output.getText().toString().length()-1);
                try {
                    String decrip = decrypt(text,-Integer.parseInt(keys));
                    output.setText(backAll4y(decrip));
                }catch (NumberFormatException e){
                    if(!input.getText().toString().equalsIgnoreCase("")){
                        Toast.makeText(MainSecretShare.this,"your input text is not encrypted text.",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        img_copy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", output.getText());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(MainSecretShare.this,"Copy output!",Toast.LENGTH_SHORT).show();
            }
        });
        img_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                input.setText("");
                output.setText("");
                key.setText("");
            }
        });
        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String txt = output.getText().toString();
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
               // share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                //share.putExtra(Intent.EXTRA_SUBJECT, "Encrypted");
                share.putExtra(Intent.EXTRA_TEXT, txt);
                startActivity(Intent.createChooser(share, "Share link!"));
            }
        });
    }
    static String enkrip(String val,int shape) {
        String k = "";
        String kk = "";
        //if (String.valueOf(shape).length() > 1) {
            if(String.valueOf(shape).length() == 1){
                k = "0";
                kk = String.valueOf(shape);
            }else if(String.valueOf(shape).length() == 2){
                k = String.valueOf(shape).substring(0, 1);
                kk = String.valueOf(shape).substring(1);
            }else if(String.valueOf(shape).length() == 3){
                k = String.valueOf(shape).substring(0, 2);
                kk = String.valueOf(shape).substring(2)+".";
            }else{
                k = String.valueOf(shape).substring(0, 2);
                kk = String.valueOf(shape).substring(2)+"..";
            }

        String combine = k + val + kk;
        char[] buffer = combine.toCharArray();
        if (kk.length()==1 && k.length()==1) {
            for (int i = 0; i < buffer.length; i++) {
                if (i == 0) {
                    char letter = buffer[i];
                    buffer[i] = letter;
                } else if (i == buffer.length - 1) {
                    char letter = buffer[i];
                    buffer[i] = letter;
                } else {
                    char letter = buffer[i];
                    letter = (char) (letter + shape);
                    buffer[i] = letter;
                }
            }
        }else if(k.length()==2 && kk.length()==2){
            for (int i = 0; i < buffer.length; i++) {
                if (i == 0) {
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else if(i==1){
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else if(i==buffer.length-2){
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else if(i==buffer.length-1){
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else{
                    char letter = buffer[i];
                    letter = (char) (letter + shape);
                    buffer[i] = letter;
                }
            }
        }else if(k.length()==2 && kk.length()==3){
            for (int i = 0; i < buffer.length; i++) {
                if (i == 0) {
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else if(i==1){
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else if(i==buffer.length-3){
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else if(i==buffer.length-2){
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else if(i==buffer.length-1){
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else{
                    char letter = buffer[i];
                    letter = (char) (letter + shape);
                    buffer[i] = letter;
                }
            }
        }else if(k.length()==2 && kk.length()==4){
            for (int i = 0; i < buffer.length; i++) {
                if (i == 0) {
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else if(i==1){
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else if(i==buffer.length-4){
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else if(i==buffer.length-3){
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else if(i==buffer.length-2){
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else if(i==buffer.length-1){
                    char letter = buffer[i];
                    buffer[i] = letter;
                }else{
                    char letter = buffer[i];
                    letter = (char) (letter + shape);
                    buffer[i] = letter;
                }
            }
        }
        return new String(buffer);
    }
    static String toAll4y(String val){
        char[] buffer = val.toCharArray();
        for (int i = 0; i <buffer.length ; i++) {
            char _4ll4yea = buffer[i];
            if(_4ll4yea=='a'||_4ll4yea=='A'){
                _4ll4yea = '4';
            }else if(_4ll4yea=='i'||_4ll4yea=='I'){
                _4ll4yea = '1';
            }else if(_4ll4yea=='z'||_4ll4yea=='Z'){
                _4ll4yea = '2';
            }else if(_4ll4yea=='e'||_4ll4yea=='E'){
                _4ll4yea = '3';
            }else if(_4ll4yea=='s'||_4ll4yea=='S'){
                _4ll4yea = '5';
            }else if(_4ll4yea=='g'||_4ll4yea=='G'){
                _4ll4yea = '6';
            }else if(_4ll4yea=='t'||_4ll4yea=='T'){
                _4ll4yea = '7';
            }else if(_4ll4yea=='b'||_4ll4yea=='B'){
                _4ll4yea = '8';
            }else if(_4ll4yea=='o'||_4ll4yea=='O'){
                _4ll4yea = '0';
            }
            buffer[i]=_4ll4yea;
        }
        return new String(buffer);
    }
    static String decrypt(String val,int shape){
        char[] buffer = val.toCharArray();
        for (int i = 0; i <buffer.length ; i++) {
            char letter = buffer[i];
            letter = (char) (letter + shape);
            buffer[i] = letter;
        }
        return new String(buffer);
    }
    static String backAll4y(String val){
        char[] buffer = val.toCharArray();
        for (int i = 0; i <buffer.length ; i++) {
            char _4ll4yea = buffer[i];
            if(_4ll4yea=='4'){
                _4ll4yea = 'a';
            }else if(_4ll4yea=='1'){
                _4ll4yea = 'i';
            }else if(_4ll4yea=='2'){
                _4ll4yea = 'z';
            }else if(_4ll4yea=='3'){
                _4ll4yea = 'e';
            }else if(_4ll4yea=='5'){
                _4ll4yea = 's';
            }else if(_4ll4yea=='6'){
                _4ll4yea = 'g';
            }else if(_4ll4yea=='7'){
                _4ll4yea = 't';
            }else if(_4ll4yea=='8'){
                _4ll4yea = 'b';
            }else if(_4ll4yea=='0'){
                _4ll4yea = 'o';
            }
            buffer[i]=_4ll4yea;
        }

        return new String(buffer);
    }
    public void castingObject(){
        lyt_content = (RelativeLayout)findViewById(R.id.lyt_content);
        lyt_content.setVisibility(View.VISIBLE);
        input = (EditText)findViewById(R.id.edit_input);
        output = (EditText)findViewById(R.id.edit_output);
        key = (EditText)findViewById(R.id.edit_key);
        btn_encrypt = (Button)findViewById(R.id.btn_encrypt);
        btn_decrypt = (Button)findViewById(R.id.btn_decrypt);
        btn_logout = (Button)findViewById(R.id.btn_logout);
        img_share = (ImageView)findViewById(R.id.img_share);
        img_copy = (ImageView)findViewById(R.id.img_copy);
        img_refresh = (ImageView)findViewById(R.id.img_refresh);
    }
}
